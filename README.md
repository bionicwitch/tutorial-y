# Tutorial Bitbucket

Primeiros passos com Bitbucket.

## Dados do aluno

(Preencha com seus dados)

- Nome completo: Yuri da Silva Rosa
- Username do Bitbucket: BionicWitch
- Email @inf: ydsrosa@inf.ufpel.edu.br
- Disciplina: AED
- Semestre: 2019/1

## Objetivo 

O objetivo desta atividade é usar pela primeira vez a configuração de _build_ do Bitbucket[1].

Nesta disciplina, Bitbucket será usado para o desenvolvimento e teste dos trabalhos. Para produzir a primeira nota dos trabalhos (sujeitas à revisão do professor), a biblioteca *simplegrade.h*[2] será usada.

O tutorial presume que o aluno está usando um computador com Linux e as ferramentas de desenvolvimento instaladas (incluindo gcc, make e git).

## Parte 1

1. Crie uma conta no Bitbucket usando seu email @inf.ufpel.edu.br e peça um desconto de estudante [aqui](https://bitbucket.org/product/education).
2. (Opcional mas recomendável) Adicione uma chave pública SSH criada no seu computador no Bitbucket no link https://bitbucket.org/account/user/mlpilla/ssh-keys/ (substitua _mlpilla_ pelo seu usuário).
3. Faça o _fork_ do repositório com a tarefa, preferencialmente usando SSH. Cuidado para marcar a opção de tornar o repositório **PRIVADO**.
![Fork me!](https://confluence.atlassian.com/bitbucket/files/304578655/933101240/2/1509043649379/fork.gif)
4. Adicione o professor (no meu caso, usuário _mlpilla_) como administrador:
![Adicionando como admin](figs/add_user.jpg)
5. Clone o repositório em um terminal (ou usando uma outra ferramenta) -- URL é o endereço que aparece na página do repositório junto com _git clone_:
    * _git clone URL_  
6. Agora você tem uma cópia do repositório local no seu computador. Modifique este README.md com seus dados.
7. Faça seu primeiro commit e envie para o Bitbucket:
    * _git add README.md_
    * _git commit -m "Preenchi dados para professor"_
    * _git push -u origin master_ 
        * Este último comando envia para o Bitbucket. 

## Parte 2

1. Escreva o código que falta no arquivo _fatorial.c_
    * __IMPORTANTE__: não altere os demais arquivos!
2. Teste executando _make_ .
3. Quando estiver satisfeito com o resultado (i.e. funciona), mude o arquivo _notbitbucket-pipelines.yml_ de nome para _bitbucket-pipelines.yml_, faça um _commit_ e envie para o Bitbucket.
    * _git mv nobitbucket-pipelines.yml bitbucket-pipelines.yml.yml_
    * _git add fatorial.c_ 
    * _git commit -m "sua mensagem aqui"_
    * _git push -u origin master_
4. O resultado do _build_ serão enviados para o professor. Também estão disponíveis no seu _dashboard_.

__IMPORTANTE__: você pode fazer quantos commits locais você quiser. Somente com o _push_ que serão enviados para o servidor. Não sobrecarregue o Bitbucket e o professor com _builds_ que você sabe que não funcionam.

## Parte 3


1. Com o arquivo de pipelines renomeado (neste tutorial) ou criado (demais tutoriais) deve-se habilitar os pipelines no dashboard do repositório
![Pipelines](figs/pipelines.png)
2. Pronto, seus resultados devem aparecer


## Parte 4

Esta parte é somente para trabalhos que devem receber nota. **Não é o caso deste tutorial.**

1. Quando o trabalho estiver pronto, crie uma _issue_ com o título "Correção do trabalho xyz" e adicione o professor como _assignee_. Primeiro, verifique se a opção de _issues_ está habilitada:
![Issues](figs/issues.jpg)
2. Depois, crie a _issue_:
![Issues](figs/issues_make.jpg)
3. ...
4. Profit!



## Referências

- [1] http://www.bitbucket.org
- [2] https://github.com/pilla/simplegrade
