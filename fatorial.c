#include "fatorial.h"

int fatorial(int x){
	int i, r=1;
	if(x==0||x<0)
	{
		return -1;
	}
	if (x==1){
		return 1;
		}
	else
	{
		 for(i = x; i > 0; i--) {
			r *= i;
		}
		return r;
	}	
	return 0;
}
